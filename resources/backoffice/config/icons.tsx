import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


export const VerifiedEmailIcon = (props: any) => (
    <FontAwesomeIcon icon={faCheckCircle} {...props} />
)
