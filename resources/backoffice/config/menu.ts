export const menuItems = [
    {
        "label": "Dashboard",
        "icon": "fa fa-tachometer-alt",
        "url": "/dashboard"
    },
    {
        "label": "Users",
        "icon": "fa fa-users",
        "url": "/users"
    },
    {
        "label": "Roles",
        "icon": "fa fa-user-tag",
        "url": "/roles"
    }
]
