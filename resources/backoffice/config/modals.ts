import UserForm from "../pages/users/UserForm";

export const modalsRegistry = {
    user: {
        Component: UserForm
    }
}
